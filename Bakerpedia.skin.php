<?php
/**
 * SkinTemplate class for the Bakerpedia skin
 *
 * @ingroup Skins
 */
class SkinBakerpedia extends SkinTemplate {
	public $skinname = 'bakerpedia', $stylename = 'Bakerpedia',
		$template = 'BakerpediaTemplate', $useHeadElement = true;

	function addToBodyAttributes( $out, &$bodyAttrs ) {
		$bodyAttrs['class'] .= 'page-template-default page page-id-1866 page-parent fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-boxed-mode menu-text-align-left fusion-woo-product-design-classic mobile-menu-design-modern fusion-image-hovers fusion-show-pagination-text do-animate';
	}
	/**
	 * Add CSS via ResourceLoader
	 *
	 * @param $out OutputPage
	 */
	public function initPage( OutputPage $out ) {

		$out->addMeta( 'viewport', 'width=device-width, initial-scale=1.0' );

		$out->addModuleStyles( array(
			'mediawiki.skinning.interface',
			'mediawiki.skinning.content.externallinks',
			'skins.bakerpedia'
		) );
		$out->addModules( array(
			'skins.bakerpedia.js'
		) );
		$out->addStyle('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css');
		$out->addStyle('//bakerpedia.com/wp-content/themes/Avada-Child-Theme/style.css?ver=1.0.0');
		$out->addStyle('//bakerpedia.com/wp-content/themes/Avada/style.css?ver=1.0.0');
		$out->addStyle('//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css');
		$out->addStyle('//bakerpedia.com/wp-content/themes/Avada/ilightbox.css?ver=1.0.0');
		$out->addStyle('//bakerpedia.com/wp-content/themes/Avada/animations.css?ver=1.0.0');
		$out->addStyle('//bakerpedia.com/wp-content/themes/Avada/assets/css/woocommerce.css?ver=1.0.0');
		

		$out->addHeadItem('bootstrap_js', '
<script type="text/javascript" src="//code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var toTopscreenReaderText = {"label":"Go to Top"};
var js_local_vars = {"admin_ajax":"\/\/bakerpedia.com\/wp-admin\/admin-ajax.php","admin_ajax_nonce":"90ad203776","protocol":"","theme_url":"\/\/bakerpedia.com\/wp-content\/themes\/Avada","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","page_smoothHeight":"false","flex_smoothHeight":"false","language_flag":"en","infinite_blog_finished_msg":"<em>All posts displayed.<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","portfolio_loading_text":"<em>Loading Portfolio Items...<\/em>","faqs_loading_text":"<em>Loading FAQ Items...<\/em>","order_actions":"Details","avada_rev_styles":"1","avada_styles_dropdowns":"1","blog_grid_column_spacing":"40","blog_pagination_type":"Pagination","carousel_speed":"2500","counter_box_speed":"1000","content_break_point":"800","disable_mobile_animate_css":"0","disable_mobile_image_hovers":"1","portfolio_pagination_type":"Pagination","form_bg_color":"#ffffff","header_transparency":"0","header_padding_bottom":"0px","header_padding_top":"0px","header_position":"Top","header_sticky":"0","header_sticky_tablet":"0","header_sticky_mobile":"0","header_sticky_type2_layout":"menu_only","sticky_header_shrinkage":"0","is_responsive":"1","is_ssl":"false","isotope_type":"masonry","layout_mode":"boxed","lightbox_animation_speed":"Fast","lightbox_arrows":"1","lightbox_autoplay":"0","lightbox_behavior":"all","lightbox_desc":"1","lightbox_deeplinking":"1","lightbox_gallery":"1","lightbox_opacity":"0.8","lightbox_path":"vertical","lightbox_post_images":"1","lightbox_skin":"light","lightbox_slideshow_speed":"5000","lightbox_social":"1","lightbox_title":"1","lightbox_video_height":"720","lightbox_video_width":"1280","logo_alignment":"Left","logo_margin_bottom":"31px","logo_margin_top":"31px","megamenu_max_width":"1100","mobile_menu_design":"modern","nav_height":"83","nav_highlight_border":"3","page_title_fading":"0","pagination_video_slide":"0","related_posts_speed":"2500","submenu_slideout":"1","side_header_break_point":"800","sidenav_behavior":"Hover","site_width":"1300px","slider_position":"below","slideshow_autoplay":"1","slideshow_speed":"7000","smooth_scrolling":"0","status_lightbox":"1","status_totop_mobile":"1","status_vimeo":"1","status_yt":"1","testimonials_speed":"4000","tfes_animation":"sides","tfes_autoplay":"1","tfes_interval":"3000","tfes_speed":"800","tfes_width":"150","title_style_type":"double","title_margin_top":"0px","title_margin_bottom":"31px","typography_responsive":"0","typography_sensitivity":"0.6","typography_factor":"1.5","woocommerce_shop_page_columns":"2","woocommerce_checkout_error":"Not all fields have been filled in correctly.","side_header_width":"0"};
/* ]]> */
</script><script type="text/javascript" src="//bakerpedia.com/wp-content/themes/Avada/assets/js/main.min.js?ver=1.0.0" async ></script> 
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
						<script>
							/* You can add more configuration options to webfontloader by previously defining the WebFontConfig with your options */
							if ( typeof WebFontConfig === "undefined" ) {
								WebFontConfig = new Object();
							}
							WebFontConfig["google"] = {families: ["PT+Sans:400,700&subset=greek-ext"]};

							(function() {
								var wf = document.createElement( "script" );
								wf.src = "//ajax.googleapis.com/ajax/libs/webfont/1.5.3/webfont.js";
								wf.type = "text/javascript";
								wf.async = "true";
								var s = document.getElementsByTagName( "script" )[0];
								s.parentNode.insertBefore( wf, s );
							})();
						</script>
'
		);
		$out->addHeadItem('inline_css', '<style>' . file_get_contents(__DIR__ . '/resources/styles.css') . '</style>');
	}

	/**
	 * @param $out OutputPage
	 */
	function setupSkinUserCss( OutputPage $out ) {
		parent::setupSkinUserCss( $out );
	}
}
