<?php
/**
 * BaseTemplate class for the Bakerpedia skin
 *
 * @ingroup Skins
 */
class BakerpediaTemplate extends BaseTemplate {
	/**
	 * Outputs the entire contents of the page
	 */
	public function execute() {
		global $wgScript, $wgUser;
		$user_links = '';
		if ( $wgUser->isLoggedIn() ) {
			$user_links = '
			<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children fusion-dropdown-menu">
				<a href="#" aria-haspopup="true">User Links</a>
				<ul class="sub-menu">
				' .
				// User profile links
					$this->getUserLinks()
				.
				// Page editing and tools
					$this->getPageLinks()
				.'
				</ul>
			<li/>
			';
		}
		$html = '';
		$this->html( 'headelement' );

		$html .= Html::rawElement( 'div', [ 'id' => 'wrapper' ],
			'<div id="home" style="position:relative;top:1px;"></div>
			<div class="fusion-header-wrapper">
				<div class="fusion-header-v3 fusion-logo-left fusion-sticky-menu-1 fusion-sticky-logo- fusion-mobile-logo- fusion-mobile-menu-design-modern ">
					<div class="fusion-header-sticky-height"></div>
<div class="fusion-header">
	<div class="fusion-row">
		<div class="fusion-logo" data-margin-top="31px" data-margin-bottom="31px" data-margin-left="0px" data-margin-right="0px">
				<a class="fusion-logo-link" href="//bakerpedia.com">
						<img src="//bakerpedia.com/wp-content/uploads/bakerpedia-logo-100x100.png" width="" height="" alt="Bakerpedia" class="fusion-logo-1x fusion-standard-logo" />

															<img src="//bakerpedia.com/wp-content/uploads/bakerpedia-logo-100x100.png" width="" height="" alt="Bakerpedia" style="max-height: px; height: auto;" class="fusion-standard-logo fusion-logo-2x" />
			
			<!-- mobile logo -->
			
			<!-- sticky header logo -->
					</a>
		</div>
		<div class="eb90"><div style="" class="">
<form action="'. $wgScript .'" id="searchform">
<div id="search-2" class="widget widget_search"><form role="search" class="searchform" method="get" action="//bakerpedia.com/">
	<div class="search-table">
		<div class="search-field">
			<input type="text" value="" name="search" data-swplive="true" data-swpengine="default" data-swpconfig="default" class="s" placeholder="Search ..." autocomplete="off">
		</div>
	</div>
	
<input type="hidden" name="lang" value="en"></form>
</div>
</form>
</div>
<a href="//bakerpedia.com/resources/the-academy/" target="_blank"><div class="eb90text">Check out our Academy classes!</div></a></div>

		<div class="fusion-main-menu"><ul id="menu-404" class="fusion-menu">' .
			// Site navigation/sidebar
				$this->getSiteNavigation()
				. $user_links . '
		</div>
		<div class="fusion-main-menu fusion-sticky-menu"><ul id="menu-main-menu-1" class="fusion-menu">' .
			// Site navigation/sidebar
				$this->getSiteNavigation()
				. $user_links . '
		</div>
<div class="fusion-mobile-menu-icons">
	<a href="#" class="fusion-icon fusion-icon-bars"></a>
</div>


<div class="fusion-mobile-nav-holder"></div>
<div class="fusion-mobile-nav-holder fusion-mobile-sticky-nav-holder"></div>
</div>
				</div>
				<div class="fusion-clearfix"></div>
			</div>
			</div>
			' .
			Html::rawElement( 'div', [ 'id' => 'main', 'class' => 'clearfix' ],
				Html::rawElement( 'h1',
					[
						'class' => 'firstHeading',
						'lang' => $this->get( 'pageLanguage' )
					],
					$this->get( 'title' )
				) .
				Html::rawElement( 'div', [ 'class' => 'fusion-row', 'id' => 'content' ], 
					Html::rawElement( 'div', [ 'class' => 'content' ], 
						Html::rawElement( 'div', [ 'class' => 'post-content' ],
							$this->get( 'bodycontent' ) .
							$this->getClear() .
							$this->getCategoryLinks()
						)
					)
				)
			) .
			$this->getFooter()
		);

		$html .= $this->getTrail();
		$html .= Html::closeElement( 'body' );
		$html .= Html::closeElement( 'html' );

		echo $html;
	}

	/**
	 * Generates the logo and (optionally) site title
	 * @return string html
	 */
	protected function getLogo( $id = 'p-logo', $imageOnly = false ) {
		$html = Html::openElement(
			'div',
			[
				'id' => $id,
				'class' => 'mw-portlet',
				'role' => 'banner'
			]
		);
		$html .= Html::element(
			'a',
			[
				'href' => $this->data['nav_urls']['mainpage']['href'],
				'class' => 'mw-wiki-logo',
			] + Linker::tooltipAndAccesskeyAttribs( 'p-logo' )
		);
		if ( !$imageOnly ) {
			$html .= Html::element(
				'a',
				[
					'id' => 'p-banner',
					'class' => 'mw-wiki-title',
					'href'=> $this->data['nav_urls']['mainpage']['href']
				] + Linker::tooltipAndAccesskeyAttribs( 'p-logo' ),
				$this->getMsg( 'sitetitle' )->escaped()
			);
		}
		$html .= Html::closeElement( 'div' );

		return $html;
	}

	/**
	 * Generates the search form
	 * @return string html
	 */
	protected function getSearch() {
		$html = Html::openElement(
			'form',
			[
				'action' => htmlspecialchars( $this->get( 'wgScript' ) ),
				'role' => 'search',
				'class' => 'mw-portlet',
				'id' => 'p-search'
			]
		);
		$html .= Html::hidden( 'title', htmlspecialchars( $this->get( 'searchtitle' ) ) );
		$html .= $this->makeSearchInput( [ 'id' => 'searchInput' ] );
		$html .= $this->makeSearchButton( 'go', [ 'id' => 'searchGoButton', 'class' => 'searchButton' ] );
		$html .= Html::closeElement( 'form' );

		return $html;
	}

	/**
	 * Generates the sidebar
	 * Set the elements to true to allow them to be part of the sidebar
	 * Or get rid of this entirely, and take the specific bits to use wherever you actually want them
	 *  * Toolbox is the page/site tools that appears under the sidebar in vector
	 *  * Languages is the interlanguage links on the page via en:... es:... etc
	 *  * Default is each user-specified box as defined on MediaWiki:Sidebar; you will still need a foreach loop
	 *    to parse these.
	 * @return string html
	 */
	protected function getSiteNavigation() {
		global $wgBakerpediaNavigation;
		$html = '';
		foreach( $wgBakerpediaNavigation as $menu_item => $menu_item_values ) {
			$contentText = '';
			if ( array_key_exists( 'sub-menu', $menu_item_values ) ) {
				$contentText = Html::openElement( 'ul', ['class' => 'sub-menu'] );
				foreach ( $menu_item_values['sub-menu'] as $sub_menu_item => $sub_menu_values ) {
					$sub_menu_contentText = '';
					if ( array_key_exists( 'sub-menu', $sub_menu_values ) ) {
						$sub_menu_contentText = Html::openElement( 'ul', ['class' => 'sub-menu'] );
						foreach( $sub_menu_values['sub-menu'] as $sub_sub_menu_item => $sub_sub_menu_values ) {
							$sub_menu_contentText .= Html::rawElement( 'li', 
								['class' => 'menu-item menu-item-type-custom menu-item-object-custom menu-item-24144'],
								Html::rawElement( 'a', ['href' => $sub_sub_menu_values['href']], $sub_sub_menu_item )
							);
						}
						$sub_menu_contentText .= Html::closeElement( 'ul' );
					}
					if ( !empty( $sub_menu_contentText ) ) {
						$sub_menu_item .= ' <i class="fa fa-caret-right fa-lg"></i>';
					}
					$contentText .= Html::rawElement( 'li', 
						['class' => 'menu-item menu-item-type-custom menu-item-object-custom menu-item-24237 fusion-dropdown-submenu'],
						Html::rawElement( 'a', ['href' => $sub_menu_values['href']], $sub_menu_item ) .
						$sub_menu_contentText
					);
				}
				$contentText .= Html::closeElement( 'ul' );
			}
			$html .= Html::rawElement( 'li', 
				['class' => 'menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children fusion-dropdown-menu'], 
				Html::rawElement( 'a', ['href' => $menu_item_values['href']], $menu_item ) . 
				$contentText
			);
		}
		return $html;
	}

	/**
	 * Generates page-related tools/links
	 * You will probably want to split this up and move all of these to somewhere that makes sense for your skin.
	 * @return string html
	 */
	protected function getPageLinks() {
		// Namespaces: links for 'content' and 'talk' for namespaces with talkpages. Otherwise is just the content.
		// Usually rendered as tabs on the top of the page.
		$html = $this->getPortlet(
			'namespaces',
			$this->data['content_navigation']['namespaces']
		);
		// Variants: Language variants. Displays list for converting between different scripts in the same language,
		// if using a language where this is applicable.
		$html .= $this->getPortlet(
			'variants',
			$this->data['content_navigation']['variants']
		);
		// 'View' actions for the page: view, edit, view history, etc
		$html .= $this->getPortlet(
			'views',
			$this->data['content_navigation']['views']
		);
		// Other actions for the page: move, delete, protect, everything else
		$html .= $this->getPortlet(
			'actions',
			$this->data['content_navigation']['actions']
		);

		return $html;
	}

	/**
	 * Generates user tools menu
	 * @return string html
	 */
	protected function getUserLinks() {
		return $this->getPortlet(
			'personal',
			$this->getPersonalTools(),
			'personaltools'
		);
	}

	/**
	 * Generates siteNotice, if any
	 * @return string html
	 */
	protected function getSiteNotice() {
		if ( $this->data['sitenotice'] ) {
			return Html::rawElement(
				'div',
				[ 'id' => 'siteNotice' ],
				$this->get( 'sitenotice' )
			);
		}
		return '';
	}

	/**
	 * Generates new talk message banner, if any
	 * @return string html
	 */
	protected function getNewTalk() {
		if ( $this->data['newtalk'] ) {
			return Html::rawElement(
				'div',
				[ 'class' => 'usermessage' ],
				$this->get( 'newtalk' )
			);
		}
		return '';
	}

	/**
	 * Generates subtitle stuff, if any
	 * @return string html
	 */
	protected function getPageSubtitle() {
		if ( $this->data['subtitle'] ) {
			return Html::rawelement (
				'p',
				[],
				$this->get( 'subtitle' )
			);
		}
		return '';
	}


	/**
	 * Generates category links, if any
	 * @return string html
	 */
	protected function getCategoryLinks() {
		if ( $this->data['catlinks'] ) {
			return $this->get( 'catlinks' );
		}
		return '';
	}

	/**
	 * Generates data after content stuff, if any
	 * @return string html
	 */
	protected function getDataAfterContent() {
		if ( $this->data['dataAfterContent'] ) {
			return $this->get( 'dataAfterContent' );
		}
		return '';
	}

	/**
	 * Generates a block of navigation links with a header
	 *
	 * @param string $name
	 * @param array|string $content array of links for use with makeListItem,
	 * or a block of text
	 * @param null|string|array|bool $msg
	 *
	 * @return string html
	 */
	protected function getPortlet( $name, $content, $msg = null ) {
		if ( $msg === null ) {
			$msg = $name;
		} elseif ( is_array( $msg ) ) {
			$msgString = array_shift( $msg );
			$msgParams = $msg;
			$msg = $msgString;
		}
		$msgObj = wfMessage( $msg );
		if ( $msgObj->exists() ) {
			if ( isset( $msgParams ) && !empty( $msgParams ) ) {
				$msgString = $this->getMsg( $msg, $msgParams )->parse();
			} else {
				$msgString = $msgObj->parse();
			}
		} else {
			$msgString = htmlspecialchars( $msg );
		}


		$labelId = Sanitizer::escapeId( "p-$name-label" );

		if ( is_array( $content ) ) {
			$name .= ' <i class="fa fa-caret-right fa-lg"></i>';
			$contentText = Html::openElement( 'ul', ['class' => 'sub-menu'] );
			foreach ( $content as $key => $item ) {
				$contentText .= $this->makeListItem(
					$key,
					$item,
					[ 'text-wrapper' => [ 'tag' => 'span' ] ]
				);
			}
			$contentText .= Html::closeElement( 'ul' );
		} else {
			$contentText = $content;
		}
		$html = Html::rawElement( 'li', 
			['class' => 'menu-item menu-item-type-custom menu-item-object-custom menu-item-24237 fusion-dropdown-submenu'], 
			Html::rawElement( 'a', [], $name ) . 
			$contentText . ''
		);

		return $html;
	}

	/* DEPRECATED FUNCTIONS: remove if you're not intending to support versions of mw under their requirements */

	/**
	 * Get a div with the core visualClear class, for clearing floats
	 *
	 * @return string html
	 * @since 1.29
	 */
	protected function getClear() {
		return Html::element( 'div', [ 'class' => 'visualClear' ] );
	}

	protected function getFooter( $iconStyle = 'icononly', $linkStyle = 'flat' ) {
		$html = '
				<div class="fusion-footer">

																
						<footer class="fusion-footer-widget-area fusion-widget-area">
							<div class="fusion-row">
								<div class="fusion-columns fusion-columns-3 fusion-widget-area">
									
																																							<div class="fusion-column col-lg-4 col-md-4 col-sm-4">
																							</div>
																																								<div class="fusion-column col-lg-4 col-md-4 col-sm-4">
												<div id="text-16" class="fusion-footer-widget-column widget widget_text">			<div class="textwidget"><div style="margin: 0 auto;"><span id="stay-connected">STAY CONNECTED</span></div></div>
		<div style="clear:both;"></div></div><div id="social_links-widget-5" class="fusion-footer-widget-column widget social_links">
		<div class="fusion-social-networks">

			<div class="fusion-social-networks-wrapper">
										<a class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook" href="https://www.facebook.com/Bakerpedia" data-placement="top" data-title="Facebook" data-toggle="tooltip" data-original-title="" title="Facebook"  target="New tab" style="font-size:24px;color:#bebdbd;"></a>

											<a class="fusion-social-network-icon fusion-tooltip fusion-twitter fusion-icon-twitter" href="https://twitter.com/BAKERpedia" data-placement="top" data-title="Twitter" data-toggle="tooltip" data-original-title="" title="Twitter"  target="New tab" style="font-size:24px;color:#bebdbd;"></a>

											<a class="fusion-social-network-icon fusion-tooltip fusion-youtube fusion-icon-youtube" href="https://www.youtube.com/channel/UC7mnOtZNoJi9i1rrbOpSa2g" data-placement="top" data-title="Youtube" data-toggle="tooltip" data-original-title="" title="Youtube"  target="New tab" style="font-size:24px;color:#bebdbd;"></a>

											<a class="fusion-social-network-icon fusion-tooltip fusion-pinterest fusion-icon-pinterest" href="https://www.pinterest.com/bakerpedia" data-placement="top" data-title="Pinterest" data-toggle="tooltip" data-original-title="" title="Pinterest"  target="New tab" style="font-size:24px;color:#bebdbd;"></a>

											<a class="fusion-social-network-icon fusion-tooltip fusion-linkedin fusion-icon-linkedin" href="https://www.linkedin.com/company/3489924" data-placement="top" data-title="Linkedin" data-toggle="tooltip" data-original-title="" title="Linkedin"  target="New tab" style="font-size:24px;color:#bebdbd;"></a>

								</div>
		</div>

		<div style="clear:both;"></div></div>																																				</div>
																																								<div class="fusion-column fusion-column-last col-lg-4 col-md-4 col-sm-4">
																							</div>
																																																																												
									<div class="fusion-clearfix"></div>
								</div> <!-- fusion-columns -->
							</div> <!-- fusion-row -->
						</footer> <!-- fusion-footer-widget-area -->
					
																
						<footer id="footer" class="fusion-footer-copyright-area fusion-footer-copyright-center">
							<div class="fusion-row">
								<div class="fusion-copyright-content">

											<div class="fusion-copyright-notice">
			<div><span style="text-align:center;">Copyright © 2018 Bakerpedia. All rights reserved. | <a href="/company/privacy-policy/">Privacy Policy</a></span></div>
		</div>
		
								</div> <!-- fusion-fusion-copyright-content -->
							</div> <!-- fusion-row -->
						</footer> <!-- #footer -->
									</div> <!-- fusion-footer -->
		';
		return $html;
	}

	/**
	 * Allows extensions to hook into known portlets and add stuff to them
	 *
	 * @param string $name
	 *
	 * @return string html
	 * @since 1.29
	 */
	protected function getAfterPortlet( $name ) {
		$html = '';
		$content = '';
		Hooks::run( 'BaseTemplateAfterPortlet', [ $this, $name, &$content ] );

		if ( $content !== '' ) {
			$html = Html::rawElement(
				'div',
				[ 'class' => [ 'after-portlet', 'after-portlet-' . $name ] ],
				$content
			);
		}

		return $html;
	}

	/**
	 * Get the basic end-page trail including bottomscripts, reporttime, and
	 * debug stuff. This should be called right before outputting the closing
	 * body and html tags.
	 *
	 * @return string
	 * @since 1.29
	 */
	function getTrail() {
		$html = MWDebug::getDebugHTML( $this->getSkin()->getContext() );
		$html .= $this->get( 'bottomscripts' );
		$html .= $this->get( 'reporttime' );

		return $html;
	}
}
